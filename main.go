package main

import (
	"fmt"

	"gitlag.com/juande.manjon/golang-ci-try/pkg/reverse"
)

func main() {
	fmt.Println(reverse.Reverse("!selpmaxe oG ,olleH"))
}
