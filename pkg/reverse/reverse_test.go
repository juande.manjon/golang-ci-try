package reverse

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestReverse(t *testing.T) {
	should_be := "123456"
	got := Reverse("654321")
	require.Equal(t, got, should_be)
}
