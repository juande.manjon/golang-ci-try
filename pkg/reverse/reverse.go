package reverse

import (
	"golang.org/x/example/stringutil"
)

func Reverse(str string) string {
	return stringutil.Reverse(str)
}
